﻿using System;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class MinHeight : MonoBehaviour {

    CharacterController _player;
    private float _prevX = 0;
    private float _prevZ = 0;
    //private readonly LinkedList<Vector2> _xlocation = new LinkedList<Vector2>();
    //private readonly LinkedList<float> _zlocation = new LinkedList<float>(); 
    private int _lag = 0;
    private StreamWriter sr;

	// Use this for initialization
	void Start () {
        _player = GetComponent<CharacterController>();
	    var pos = _player.transform.position;
        RenderSettings.fog = false;

        // Spawn player at random point in water
        var rando = new System.Random();
        pos.x = rando.Next(-47, 47) + 262;
	    pos.z = rando.Next(-47, 47) + 233;
        _player.transform.position = new Vector3(pos.x, pos.y, pos.z);

        // Create log file for reading positions
	    /*if (File.Exists("Position.txt"))
	    {
	        Debug.Log("Position.txt already exists");
	    }
	    else
	    {
	        File.CreateText("Position.txt");
	    }*/
	}
	
	// Update is called once per frame
	void Update () {

        // Makes you unable to go below water
        if (_player.transform.position.y < 115.44f)
        {
            _player.transform.position = new Vector3(_player.transform.position.x, 115.44f, _player.transform.position.z);
        }

        // Checks to see if player is trying to get out of water
	    if (Mathf.Pow(_player.transform.position.x - 262, 2) + Mathf.Pow(_player.transform.position.z - 233, 2) > 2304)
	    {
	        _player.transform.position = new Vector3(_prevX, 115.44f, _prevZ);
	    }
	    else // Update last known viable position for movement
	    {
	        _prevX = _player.transform.position.x;
	        _prevZ = _player.transform.position.z;
	    }

        
	    if (_lag % 30 == 0)
	    {
            //sr.WriteLine((_player.transform.position.x - 262) + " " + (_player.transform.position.z - 233));
	        //_xlocation.AddLast(new Vector2(_player.transform.position.x - 262, _player.transform.position.z - 233)); // Records data for x-position
            Debug.Log("X: " + (_player.transform.position.x - 262) + "\nZ: " + (_player.transform.position.z - 233) + "\n\n");
            //_zlocation.AddLast(_player.transform.position.)
	    }
	    _lag++;

	    // Records data for head tracking
	}
}
